import { http } from './http'

const baseUrl = '/api/order'

export const OrderApi = {
    getOrders () {
        return http.get(baseUrl)
    },

    getOrderById (id) {
        return http.get(baseUrl + '/' + id)
    },

    addOrder (order) {
        return http.post(baseUrl, order)
    },

    pay (id) {
        return http.patch(baseUrl + '/pay/' + id)
    },

    complete (id) {
        return http.patch(baseUrl + '/complete/' + id)
    },

    cancel (id) {
        return http.patch(baseUrl + '/cancel/' + id)
    },

    delete (id) {
        return http.delete(baseUrl + '/' + id)
    }
}