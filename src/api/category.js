import { http } from './http'

const baseUrl = '/api/category'

export const CategoryApi = {
    getCategories () {
        return http.get(baseUrl)
    },

    addCategory (name) {
        return http.post(baseUrl, {
            'name': name
        })
    },

    delCategory (id) {
        return http.delete(baseUrl + '/' + id)
    },

    editCategory (id,name) {
        return http.put(baseUrl + '/' + id, {
            'name': name
        })
    }
}