import { http } from './http'

const baseUrl = '/api/userCenter/group'

export const GroupApi = {
    getGroups () {
        return http.get(baseUrl)
    },

    addGroup (group) {
        return http.post(baseUrl, {
            'name': group.Name,
            'description': group.Description
        })
    },

    delGroup (name) {
        return http.delete(baseUrl + '/' + name)
    },

}