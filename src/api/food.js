import { http } from './http'

const baseUrl = '/api/food'

export const FoodApi = {
    getFoods () {
        return http.get(baseUrl)
    },

    getFoodsByCategory (id) {
        return http.get(baseUrl + '/category/' + id)
    },

    addFood (food) {
        return http.post(baseUrl,food)
    },

    editFood (id,food) {
        return http.put(baseUrl + '/' + id,food)
    },

    delFood (id) {
        return http.delete(baseUrl + '/' + id)
    },
}