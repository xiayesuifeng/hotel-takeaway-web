import { http } from './http'

const baseUrl = '/api/address'

export const AddressApi = {
    getAddress () {
        return http.get(baseUrl)
    },

    getAddressById (id) {
        return http.get(baseUrl + '/' + id)
    },

    addAddress(address) {
        return http.post(baseUrl,address)
    },

    editAddress(id,address) {
        return http.put(baseUrl + '/' + id,address)
    },

    deleteAddress(id) {
        return http.delete(baseUrl + '/' + id)
    }
}