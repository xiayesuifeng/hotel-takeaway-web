import { http } from './http'

const baseUrl = '/api/userCenter/user'

export const UserApi = {
    getUsers () {
        return http.get(baseUrl)
    },

    delUser (id) {
        return http.delete(baseUrl + '/' + id)
    },

}