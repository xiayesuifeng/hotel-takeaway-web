import { http } from './http'

export const AdminApi = {
    login (username, password) {
        return http.post('/api/login', {
            'username': username,
            'password': password,
        })
    },

    signup (email,nickname,username,password) {
        return http.post('/api/signup',{
            'email': email,
            'nickname': nickname,
            'username': username,
            'password': password,
        })
    },

    logout() {
        return http.post('/api/logout')
    },
}