import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import CardActions from '@material-ui/core/CardActions'
import IconButton from '@material-ui/core/IconButton'
import AddCircleSharpIcon from '@material-ui/icons/AddCircleSharp'
import React, { useContext } from 'react'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { ShoppingCartContext } from '../pages/Takeaway'

const useStyles = makeStyles(theme => ({
    card: {
        width: 345,
            margin: theme.spacing(5),
            display: 'flex',
            justifyContent: 'space-between',
            flexDirection: 'column'
    },
    media: {
        height: 140,
    },
    cardActions: {
        display: 'flex',
            justifyContent: 'space-between',
    },
}))

export default function Food (props) {
    const classes = useStyles()

    const {shoppingCartDispatch} = useContext(ShoppingCartContext)

    return (
        <Card className={classes.card}>
            <CardActionArea>
                <CardMedia
                    className={classes.media}
                    image={props.food.image}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {props.food.name}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {props.food.description}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions className={classes.cardActions}>
                <Typography>
                    ￥ {props.food.price}
                </Typography>
                <IconButton onClick={() => shoppingCartDispatch({type:'ADD_FOOD',data:props.food})}>
                    <AddCircleSharpIcon color="primary"/>
                </IconButton>
            </CardActions>
        </Card>
    )
}