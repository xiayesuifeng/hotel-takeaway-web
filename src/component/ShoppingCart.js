import useTheme from '@material-ui/core/styles/useTheme'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import React, { useContext, useEffect, useState } from 'react'
import { AddressApi } from '../api/address'
import { OrderApi } from '../api/order'
import Paper from '@material-ui/core/Paper'
import Snackbar from '@material-ui/core/Snackbar'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import TextField from '@material-ui/core/TextField'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import Avatar from '@material-ui/core/Avatar'
import AddIcon from '@material-ui/icons/Add'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'
import Box from '@material-ui/core/Box'
import RemoveCircleSharpIcon from '@material-ui/icons/RemoveCircleSharp'
import { ShoppingCartContext } from '../pages/Takeaway'
import { makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
    shoppingCart: {
        zIndex: 99999,
        width: 350,
        position: 'fixed',
        bottom: theme.spacing(3),
        right: theme.spacing(3),
        margin: theme.spacing(1),
        borderRadius: 25,
    },
    header: {
        borderRadius: 'inherit',
        backgroundColor: theme.palette.primary.main,
        display: 'flex',
        justifyContent: 'space-between',
        color: theme.palette.background.default,
        paddingLeft: 20,
        alignItems: 'center',
        marginBottom: 10
    },
    img: {
        width: 80,
        height: 80,
    },
    removeButton: {
        display: 'flex',
        alignItems: 'center',
        paddingRight: theme.spacing(1)
    },
    end: {
        marginTop: 10,
        borderRadius: 'inherit',
        backgroundColor: '#c5c5c5',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    }
}))

export default function ShoppingCart (props) {
    const classes = useStyles()
    const theme = useTheme()
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'))

    const [address, setAddress] = useState([])
    const [price, setPrice] = useState(0)
    const [dialogOpen, setDialogOpen] = useState(false)
    const [addressDialogOpen, setAddressDialogOpen] = useState(false)
    const [newAddress, setNewAddress] = useState({})
    const [snackbar, setSnackbar] = useState({open: false, message: ''})

    const {shoppingCart,shoppingCartDispatch} = useContext(ShoppingCartContext)

    useEffect(() => {
        getAddress()
    }, [])

    useEffect(() => {
        calcPrice()
    }, [shoppingCart])

    const getAddress = () => AddressApi.getAddress().then(data => {setAddress(data.address)})

    const calcPrice = () => {
        let count = 0
        for (let i = 0; i < shoppingCart.length; i++) {
            count += shoppingCart[i].price
        }

        setPrice(count)
    }

    const settlement = id => () => {
        setDialogOpen(false)
        OrderApi.addOrder({
            'addressId': id,
            'foods': shoppingCart,
        })
            .then(() => {
                shoppingCartDispatch({type:'RESET'})
                props.history.push('/order')
            })
            .catch(err => {
                setSnackbar({open: true, message: err.toString()})
            })
    }

    const handleSettlement = () => {
        setDialogOpen(true)
    }

    const handleAddAddress = () => {
        AddressApi.addAddress(newAddress).then(() => {
            getAddress()
        })
        setAddressDialogOpen(false)
    }

    return (
        <Paper className={classes.shoppingCart}>
            <Snackbar
                anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                open={snackbar.open}
                onClose={() => setSnackbar({open: false, message: ''})}
                ContentProps={{
                    'aria-describedby': 'message-id',
                }}
                message={snackbar.message}
            />
            <Dialog open={addressDialogOpen} onClose={() => setAddressDialogOpen(false)}>
                <DialogTitle>添加新配送地址</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin="dense"
                        label="姓名"
                        type="text"
                        fullWidth
                        defaultValue={newAddress.name}
                        onChange={(event) => setNewAddress({...newAddress, name: event.target.value})}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        label="年龄"
                        type="number"
                        fullWidth
                        defaultValue={newAddress.age}
                        onChange={(event) => setNewAddress({...newAddress, age: event.target.value})}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        label="手机号"
                        type="number"
                        fullWidth
                        defaultValue={newAddress.phone}
                        onChange={(event) => setNewAddress({...newAddress, phone: event.target.value})}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        label="地址"
                        type="text"
                        fullWidth
                        defaultValue={newAddress.address}
                        onChange={(event) => setNewAddress({...newAddress, address: event.target.value})}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setAddressDialogOpen(false)} color="primary">
                        取消
                    </Button>
                    <Button onClick={handleAddAddress} color="primary">
                        添加
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog onClose={() => setDialogOpen(false)} fullScreen={fullScreen} open={dialogOpen}>
                <DialogTitle>选择配送地址</DialogTitle>
                <List>
                    {address.map(address => (
                        <ListItem button onClick={settlement(address.ID)} key={address.ID}>
                            <ListItemText primary={address.name + ':' + address.address}/>
                        </ListItem>
                    ))}

                    <ListItem autoFocus button onClick={() => setAddressDialogOpen(true)}>
                        <ListItemAvatar>
                            <Avatar>
                                <AddIcon/>
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="添加新配送地址"/>
                    </ListItem>
                </List>
            </Dialog>
            <div className={classes.header}>
                <Typography>购物车</Typography>
                <IconButton>
                    <CloseIcon onClick={props.onClose}/>
                </IconButton>
            </div>
            {shoppingCart.length === 0 ? <Typography>空购物车</Typography>
                : shoppingCart.map((food, index) =>
                    (<Box display='flex'>
                            <img className={classes.img} src={food.image} alt=""/>
                            <Box display='flex' justifyContent='space-between' flexGrow='1'>
                                <Box display='flex' pl={1} flexDirection='column' justifyContent='space-between'>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {food.name}
                                    </Typography>
                                    <Typography>
                                        ￥ {food.price}
                                    </Typography>
                                </Box>
                                <div className={classes.removeButton}>
                                    <IconButton>
                                        <RemoveCircleSharpIcon color="primary" onClick={() => shoppingCartDispatch({type:'REMOVE_FOOD',data: index})}/>
                                    </IconButton>
                                </div>
                            </Box>
                        </Box>
                    ))}
            <div className={classes.end}>
                <Typography>
                    ￥ {price}
                </Typography>
                <Button variant="contained" color="primary" disabled={shoppingCart.length === 0}
                        onClick={handleSettlement}>
                    结算
                </Button>
            </div>
        </Paper>
    )
}