import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import {BrowserRouter as Router} from 'react-router-dom'
import {MuiThemeProvider,createMuiTheme} from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#6ec6ff',
            main: '#2196F3',
            dark: '#0069c0',
            contrastText: '#FFF',
        },
        secondary: {
            light: '#ff616f',
            main: '#ff1744',
            dark: '#c4001d',
            contrastText: '#FFF',
        },
    },
});

ReactDOM.render(
    <MuiThemeProvider theme={theme}>
        <Router>
            <App/>
        </Router>
    </MuiThemeProvider>, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
