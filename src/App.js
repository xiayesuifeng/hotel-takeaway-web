import React, { Suspense } from 'react'
import { Route, Switch } from 'react-router-dom'
import './App.css';
import CssBaseline from '@material-ui/core/CssBaseline'
import LoadingFrame from './component/LoadingFrame'

const Frame = React.lazy(() => import('./pages/Frame'))
const Login = React.lazy(() => import('./pages/Login'))
const Signup = React.lazy(() => import('./pages/Signup'))
const Admin = React.lazy(() => import('./pages/admin/Admin'))

function App() {
  return (
    <div className="App">
      <CssBaseline/>
      <Suspense fallback={<LoadingFrame/>}>
          <Switch>
            <Route path="/admin" component={Admin}/>
            <Route path="/login" component={Login}/>
            <Route path="/signup" component={Signup}/>
            <Route path="/" component={Frame}/>
          </Switch>
      </Suspense>
    </div>
  );
}

export default App;
