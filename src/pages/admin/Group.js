import React, { Component } from 'react'
import {
    Button,
    Dialog, DialogActions, DialogContent, DialogTitle,
    IconButton,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    TableSortLabel, TextField,
    Tooltip
} from '@material-ui/core'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import DeleteIcon from '@material-ui/icons/Delete'
import AddIcon from '@material-ui/icons/Add'
import { GroupApi } from '../../api/group'

const styles = theme => ({})

class Group extends Component {
    state = {
        groups: [],
        group: {},
        dialogTitle: '',
        dialogContext: '',
        dialogOpen: false
    }

    componentDidMount () {
        this.getGroups()
    }

    getGroups () {
        GroupApi.getGroups()
            .then(data => {
                this.setState({groups: data.groups})
            })
    }

    handleAdd = () => {
        if (this.state.dialogOpen === true) {
            GroupApi.addGroup(this.state.group)
                .then(() => {
                    this.setState({dialogOpen: false})
                    this.getGroups()
                })
                .catch(err => {
                    console.log(err)
                })
        } else {
            this.setState({dialogOpen: true, group: {}})
        }
    }

    handleDel = name => () => {
        GroupApi.delGroup(name)
            .then(() => {
                this.getGroups()
            })
            .catch(err => {

            })
    }

    render () {
        return (
            <div className="Group">
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                <Tooltip
                                    title="排序"
                                    enterDelay={300}
                                >
                                    <TableSortLabel>
                                        群组名
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                            <TableCell>
                                <Tooltip
                                    title="排序"
                                    enterDelay={300}
                                >
                                    <TableSortLabel>
                                        描述
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                            <TableCell>
                                <IconButton aria-label="Add" onClick={this.handleAdd}>
                                    <AddIcon/>
                                </IconButton>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.groups.map((group) => {
                            return (
                                <TableRow key={group.ID}>
                                    <TableCell>
                                        {group.Name}
                                    </TableCell>
                                    <TableCell>
                                        {group.Description}
                                    </TableCell>
                                    <TableCell>
                                        <IconButton aria-label="Delete" onClick={this.handleDel(group.Name)}>
                                            <DeleteIcon/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            )
                        }, this)}
                    </TableBody>
                </Table>
                <Dialog
                    open={this.state.dialogOpen}
                    onClose={() => this.setState({dialogOpen: false})}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">新建群组</DialogTitle>
                    <DialogContent>
                        <TextField autoFocus required margin="normal" id="name" label="群组名" type="text"
                                   onChange={(event) => this.setState({
                                       group: {
                                           ...this.state.group,
                                           Name: event.target.value
                                       }
                                   })}
                                   defaultValue={this.state.group.Name}
                                   fullWidth/>
                        <TextField autoFocus required margin="normal" id="description" label="描述" type="text"
                                   onChange={(event) => this.setState({
                                       group: {
                                           ...this.state.group,
                                           Description: event.target.value
                                       }
                                   })}
                                   defaultValue={this.state.group.Description}
                                   fullWidth/>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({dialogOpen: false})} color="primary">
                            取消
                        </Button>
                        <Button onClick={this.handleAdd} color="primary" autoFocus>
                            确定
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

Group.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(Group)
