import React, { useEffect, useState } from 'react'
import { FoodApi } from '../../api/food'
import Box from '@material-ui/core/Box'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import CardActions from '@material-ui/core/CardActions'
import IconButton from '@material-ui/core/IconButton'
import Card from '@material-ui/core/Card'
import makeStyles from '@material-ui/core/styles/makeStyles'
import AddIcon from '@material-ui/icons/Add'
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'
import Fab from '@material-ui/core/Fab'
import DialogTitle from '@material-ui/core/DialogTitle'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import { CategoryApi } from '../../api/category'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import Snackbar from '@material-ui/core/Snackbar'

const useStyles = makeStyles(theme => ({
    root: {
        height: '100%',
    },
    card: {
        width: 345,
        margin: theme.spacing(5),
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'column'
    },
    media: {
        height: 140,
    },
    cardActions: {
        display: 'flex',
        justifyContent: "space-between",
    },
    fab: {
        zIndex: 99999,
        position: 'fixed',
        bottom: theme.spacing(3),
        right: theme.spacing(3),
    },
}))

export default function Food (props) {
    const classes = useStyles()

    const [foods, setFoods] = useState([])
    const [categories, setCategories] = useState([])
    const [food,setFood] = useState({image:'',name:'',description:'',price:0,categoryId:1})
    const [editID,setEditID] = useState(-1)
    const [dialogOpen,setDialogOpen] = useState(false)
    const [snackbar,setSnackbar] = useState({open:false,message:''})

    useEffect(() => {
        getCategories()
        getFoods()
    },[])

    const getCategories = () => CategoryApi.getCategories().then(data => setCategories(data.categories))
    const getFoods = () => FoodApi.getFoods().then(data => setFoods(data.foods))

    const handleEdit = food => () => {
        setEditID(food.ID)
        setFood(food)
        setDialogOpen(true)
    }

    const handleAdd = () => {
        if (editID !== -1) {
            setEditID(-1)
            setFood({image:'',name:'',description:'',price:0,categoryId: categories[0].ID})
        }

        setDialogOpen(true)
    }

    const handleDialogAdd = () => {
        if (editID === -1) {
            FoodApi.addFood(food)
                .then(() =>{
                    setFood({image:'',name:'',description:'',price:0,categoryId: categories[0].ID})
                    getFoods()
                    setSnackbar({open:true,message:'添加成功'})
                })
                .catch(err => {
                    setSnackbar({open:true,message:'添加失败! ' + err.toString()})
                })
        } else {
            FoodApi.editFood(editID,food)
                .then(() =>{
                    setFood({image:'',name:'',description:'',price:0,categoryId: categories[0].ID})
                    getFoods()
                    setSnackbar({open:true,message:'编辑成功'})
                })
                .catch(err => {
                    setSnackbar({open:true,message:'编辑失败! ' + err.toString()})
                })
        }
        setDialogOpen(false)
    }

    const handleDelete = id => () => {
        FoodApi.delFood(id)
            .then(() =>{
                getFoods()
                setSnackbar({open:true,message:'删除成功'})
            })
            .catch(err => {
                setSnackbar({open:true,message:'删除失败! ' + err.toString()})
            })
    }

    return (
        <div className={classes.root}>
            <Snackbar
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={snackbar.open}
                autoHideDuration={4000}
                onClose={() => setSnackbar({open:false,message:''})}
                ContentProps={{
                    'aria-describedby': 'message-id',
                }}
                message={<span id="message-id">{snackbar.message}</span>}
            />
            <Fab color="primary" className={classes.fab}>
                <AddIcon onClick={handleAdd} />
            </Fab>
            <Box display="flex" flexWrap="wrap">
                {foods.map(food => (
                    <Card className={classes.card}>
                    <CardActionArea>
                        <CardMedia
                            className={classes.media}
                            image={food.image}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {food.name}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                {food.description}
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                    <CardActions className={classes.cardActions}>
                        <Typography>
                            ￥ {food.price}
                        </Typography>
                        <div>
                            <IconButton>
                                <EditIcon color="primary" onClick={handleEdit(food)}/>
                            </IconButton>
                            <IconButton>
                                <DeleteIcon color="primary" onClick={handleDelete(food.ID)}/>
                            </IconButton>
                        </div>
                    </CardActions>
                </Card>))}
            </Box>
            <Dialog onClose={() =>setDialogOpen(false)} open={dialogOpen}>
                <DialogTitle onClose={() => setDialogOpen(false)}>
                    {editID === -1 ? '添加食物' : '编辑食物'}
                </DialogTitle>
                <DialogContent dividers>
                    <TextField
                        autoFocus
                        margin="dense"
                        label="图片"
                        type="text"
                        fullWidth
                        defaultValue={food.image}
                        onChange={(event) => setFood({...food,image: event.target.value})}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        label="名字"
                        type="text"
                        fullWidth
                        defaultValue={food.name}
                        onChange={(event) => setFood({...food,name: event.target.value})}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        label="描述"
                        type="text"
                        fullWidth
                        defaultValue={food.description}
                        onChange={(event) => setFood({...food,description: event.target.value})}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        label="价格"
                        type="number"
                        fullWidth
                        defaultValue={food.price}
                        onChange={(event) => setFood({...food,price: parseInt(event.target.value)})}
                    />
                    <FormControl>
                        <InputLabel>分类</InputLabel>
                        <Select
                            value={food.categoryId}
                            onChange={event => setFood({...food,categoryId:event.target.value})}
                        >
                            {categories.map(category => <MenuItem value={category.ID}>{category.name}</MenuItem>)}
                        </Select>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={() => setDialogOpen(false)} color="primary">
                        取消
                    </Button>
                    <Button autoFocus onClick={handleDialogAdd} color="primary">
                        {editID === -1 ? '添加' : '编辑'}
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}