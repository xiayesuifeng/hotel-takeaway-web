import React, { Component } from 'react'
import {
    Button,
    Checkbox, Dialog, DialogActions, DialogContent, DialogTitle,
    IconButton, List, ListItem, ListItemText, ListSubheader,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    TableSortLabel, TextField,
    Tooltip
} from '@material-ui/core'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'
import AddIcon from '@material-ui/icons/Add'

import { GroupApi } from '../../api/group'
import { UserApi } from '../../api/user'

const styles = theme => ({})

class User extends Component {

    state = {
        users: [],
        groups: [],
        user: {},
        checkedList: [],
        dialogTitle: '',
        dialogContext: '',
        dialogOpen: '',
        dialogEdit: false
    }

    componentDidMount () {
        this.getUsers()
        this.getGroups()
    }

    getUsers () {
        UserApi.getUsers()
            .then(data => {
                this.setState({users: data.users})
            })
    }

    getGroups () {
        GroupApi.getGroups()
            .then(data => {
                this.setState({groups: data.groups})
            })
    }

    getGroupList () {
        let list = []
        for (let i in this.state.checkedList) {
            if (this.state.checkedList[i].checked) {
                list.push(this.state.checkedList[i].Name)
            }
        }
        return list
    }

    handleEditGroup = list => () => {
        const newList = [...this.state.checkedList]
        for (let i in newList) {
            if (newList[i].Name === list.Name) {
                newList[i].checked = !list.checked
                break
            }
        }

        this.setState({checkedList: newList})
    }

    handleAdd = () => {
        if (this.state.dialogOpen === true) {

        } else {
            let list = []
            for (let i in this.state.groups) {
                list.push({Name: this.state.groups[i].Name, checked: false})
            }
            this.setState({dialogOpen: true, checkedList: list, user: {}})
        }
    }

    handleEdit = user => () => {
        if (this.state.dialogOpen === true) {

        } else {
            let list = []
            for (let i in this.state.groups) {
                let tmp = false
                for (let j in user.Group) {
                    if (user.Group[j].Name === this.state.groups[i].Name) {
                        tmp = true
                        break
                    }
                }
                list.push({Name: this.state.groups[i].Name, checked: tmp})
            }

            this.setState({user: user, checkedList: list, dialogOpen: true, dialogEdit: true})
        }
    }

    render () {
        return (
            <div>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                <Tooltip
                                    title="排序"
                                    enterDelay={300}
                                >
                                    <TableSortLabel>
                                        昵称
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                            <TableCell>
                                <Tooltip
                                    title="排序"
                                    enterDelay={300}
                                >
                                    <TableSortLabel>
                                        用户名
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                            <TableCell>
                                <Tooltip
                                    title="排序"
                                    enterDelay={300}
                                >
                                    <TableSortLabel>
                                        邮箱
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                            <TableCell>
                                <Tooltip
                                    title="排序"
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                    >
                                        群组
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                            <TableCell>
                                <IconButton aria-label="Add" onClick={this.handleAdd}>
                                    <AddIcon/>
                                </IconButton>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.users.map((user) => {
                            return (
                                <TableRow key={user.ID}>
                                    <TableCell>
                                        {user.Nickname}
                                    </TableCell>
                                    <TableCell>
                                        {user.Username}
                                    </TableCell>
                                    <TableCell>
                                        {user.Email}
                                    </TableCell>
                                    <TableCell>
                                        {user.Group.map((g) => {
                                            return (
                                                g.Name + ','
                                            )
                                        }, this)}
                                    </TableCell>
                                    <TableCell>
                                        <IconButton aria-label="Edit">
                                            <EditIcon onClick={this.handleEdit(user)}/>
                                        </IconButton>
                                        <IconButton aria-label="Delete">
                                            <DeleteIcon/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            )
                        }, this)}
                    </TableBody>
                </Table>
                <Dialog
                    open={this.state.dialogOpen}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{this.state.dialogEdit ? '编辑用户' : '新建用户'}</DialogTitle>
                    <DialogContent>
                        <TextField autoFocus required margin="normal" id="nickname" label="昵称" type="text"
                                   onChange={(event) => this.setState({
                                       user: {
                                           ...this.state.user,
                                           Nickname: event.target.value
                                       }
                                   })}
                                   defaultValue={this.state.user.Nickname}
                                   fullWidth/>
                        <TextField autoFocus required margin="normal" id="username" label="用户名" type="text"
                                   onChange={(event) => this.setState({
                                       user: {
                                           ...this.state.user,
                                           Username: event.target.value
                                       }
                                   })}
                                   defaultValue={this.state.user.Username}
                                   fullWidth/>
                        <TextField autoFocus required margin="normal" id="password" label="密码" type="password"
                                   onChange={(event) => this.setState({
                                       user: {
                                           ...this.state.user,
                                           Password: event.target.value
                                       }
                                   })}
                                   defaultValue={this.state.user.Password}
                                   fullWidth/>
                        <TextField autoFocus required margin="normal" id="email" label="邮箱" type="email"
                                   onChange={(event) => this.setState({
                                       user: {
                                           ...this.state.user,
                                           Email: event.target.value
                                       }
                                   })}
                                   defaultValue={this.state.user.Email}
                                   fullWidth/>
                        <List subheader={<ListSubheader>所属群组</ListSubheader>}>
                            {this.state.checkedList.map(list => (
                                <ListItem key={list.Name}
                                          dense button
                                          onClick={this.handleEditGroup(list)}
                                >
                                    <ListItemText primary={list.Name}/>
                                    <Checkbox checked={list.checked}/>
                                </ListItem>
                            ))}
                        </List>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({dialogOpen: false})} color="primary">
                            取消
                        </Button>
                        {this.state.dialogEdit ?
                            <Button color="primary" onClick={this.handleEdit} autoFocus>
                                确定
                            </Button> :
                            <Button color="primary" onClick={this.handleAdd} autoFocus>
                                确定
                            </Button>
                        }
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

User.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(User)
