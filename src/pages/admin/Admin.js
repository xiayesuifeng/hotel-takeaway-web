import React, {Component} from 'react';
import User from "./User";
import Group from "./Group";
import Category from './Category'
import Food from './Food'
import {
    Typography,
    AppBar,
    Collapse,
    Divider,
    Drawer,
    ListItemText, MenuItem,
    MenuList,
    Toolbar
} from "@material-ui/core";
import { Link, Route, Switch } from 'react-router-dom'
import { ExpandLess, ExpandMore } from '@material-ui/icons'
import {withStyles} from '@material-ui/core/styles';
import Cookies from 'js-cookie';
import Button from '@material-ui/core/Button'
import Hidden from '@material-ui/core/Hidden'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'

const drawerWidth = 200

const styles = theme => ({
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    toolbar: theme.mixins.toolbar,
    menuItem: {
        '&:focus': {
            backgroundColor: theme.palette.primary.main,
            '& $primary, & $icon': {
                color: theme.palette.common.white,
            },
        },
    },
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
    },
    root: {
        display: 'flex',
        flex: 1,
    },
    title: {
        textAlign: 'left',
        flexGrow: 1
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
});

const router = ['/admin/category','/admin/food', '/admin/uCenter/user', '/admin/uCenter/group']

class Admin extends Component {
    state = {
        UCOpen: true,
        mobileOpen: false,
    };

    componentWillMount () {
        let login = Cookies.getJSON('user') !== undefined
        if (!login) {
            this.props.history.push('/login')
            return
        }

        if (this.props.location.pathname === '/admin' || this.props.location.pathname === '/')
            this.props.history.push(router[0])
    }

    handleDrawerToggle = () => {
        this.setState(state => ({mobileOpen: !state.mobileOpen}))
    }

    handleExit = () => {
        this.props.history.push('/');
    }

    render() {
        const {classes} = this.props;

        const drawer = (
            <div>
                <div className={classes.toolbar}>
                </div>
                <Divider/>
                <MenuList>
                    <MenuItem className={classes.menuItem} selected={this.props.location.pathname === router[0]} component={Link} to={router[0]}>
                        <ListItemText inset primary="分类" />
                    </MenuItem>
                    <MenuItem className={classes.menuItem} selected={this.props.location.pathname === router[1]} component={Link} to={router[1]}>
                        <ListItemText inset primary="食物" />
                    </MenuItem>
                    <MenuItem onClick={() => {
                        this.setState({UCOpen: !this.state.UCOpen})
                    }}>
                        <ListItemText primary="用户中心"/>
                        {this.state.UCOpen ? <ExpandLess/> : <ExpandMore/>}
                    </MenuItem>
                    <Collapse in={this.state.UCOpen} timeout="auto" unmountOnExit>
                        <MenuItem className={classes.menuItem} selected={this.props.location.pathname === router[2]} component={Link} to={router[2]}>
                            <ListItemText inset primary="用户" />
                        </MenuItem>
                        <MenuItem className={classes.menuItem} selected={this.props.location.pathname === router[3]} component={Link} to={router[3]}>
                            <ListItemText inset primary="群组" />
                        </MenuItem>
                    </Collapse>
                </MenuList>
            </div>
        )

        return (
            <div className={classes.root}>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}
                        >
                            <MenuIcon/>
                        </IconButton>
                        <Typography className={classes.title} variant="h6" color="inherit" noWrap>
                            后台管理
                        </Typography>
                        <Button color="inherit" onClick={this.handleExit}>退出后台</Button>
                    </Toolbar>
                </AppBar>
                <nav className={classes.drawer}>
                    <Hidden smUp implementation="css">
                        <Drawer
                            container={this.props.container}
                            variant="temporary"
                            open={this.state.mobileOpen}
                            onClose={this.handleDrawerToggle}
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                            ModalProps={{
                                keepMounted: true,
                            }}
                        >
                            {drawer}
                        </Drawer>
                    </Hidden>
                    <Hidden xsDown implementation="css">
                        <Drawer
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                            variant="permanent"
                            open
                        >
                            {drawer}
                        </Drawer>
                    </Hidden>
                </nav>
                <main className={classes.content}>
                    <div className={classes.toolbar}/>
                    <Switch>
                        <Route exact path="/admin/category" component={Category}/>
                        <Route exact path="/admin/food" component={Food}/>
                        <Route path="/admin/uCenter/user" component={User}/>
                        <Route path="/admin/uCenter/group" component={Group}/>
                    </Switch>
                </main>
            </div>
        )
    }
}

export default withStyles(styles)(Admin);
