import React, { useEffect, useState } from 'react'
import Snackbar from '@material-ui/core/Snackbar'
import TextField from '@material-ui/core/TextField'
import InputAdornment from '@material-ui/core/InputAdornment'
import IconButton from '@material-ui/core/IconButton'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import DoneIcon from '@material-ui/icons/Done'
import AddIcon from '@material-ui/icons/Add'
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'
import { CategoryApi } from '../../api/category'
import makeStyles from '@material-ui/core/styles/makeStyles'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(5),
        display: 'inline-flex',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        justifyItems: 'center'
    }
}))

function Category (props) {

    const classes = useStyles()

    const [categories, setCategories] = useState([])
    const [snackbar,setSnackbar] = useState({open:false,message:''})
    const [addCategoryName,setAddCategoryName] = useState('')
    const [editID,setEditID] = useState(-1)

    useEffect(() => {
        getCategories()
    },[])

    const getCategories = () => CategoryApi.getCategories().then(data => setCategories(data.categories))

    const addCategory = () => {
        if (addCategoryName !== '') {
            CategoryApi.addCategory(addCategoryName)
                .then(() => {
                    setAddCategoryName('')
                    setSnackbar({open:true,message:'添加成功'})
                    getCategories()
                })
                .catch(err => {
                    setSnackbar({open:true,message:err.toString()})
                })
        }
    }

    const delCategory = id => () => {
        CategoryApi.delCategory(id)
            .then(() => {
                setSnackbar({open:true,message:'删除成功'})
                getCategories()
            })
            .catch(err => {
                setSnackbar({open:true,message:err.toString()})
            })
    }

    const editCategory = () => {
        CategoryApi.editCategory(editID, addCategoryName)
            .then(() => {
                setAddCategoryName('')
                setEditID(-1)
                setSnackbar({open:true,message:'编辑成功'})
                getCategories()
            })
            .catch(err => {
                setSnackbar({open:true,message:err.toString()})
            })
    }

    const handleEdit = category => () => {
        setAddCategoryName(category.name)
        setEditID(category.ID)
    }

    return (
        <div className={classes.root}>
            <Snackbar
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={snackbar.open}
                autoHideDuration={4000}
                onClose={() => setSnackbar({open:false,message:''})}
                ContentProps={{
                    'aria-describedby': 'message-id',
                }}
                message={<span id="message-id">{snackbar.message}</span>}
            />
            <TextField
                id="category"
                label='分类名'
                type="text"
                value={addCategoryName}
                onChange={e => setAddCategoryName(e.target.value)}
                margin="normal"
                InputProps={{
                    endAdornment: <InputAdornment position="start">
                        {editID === -1 ?
                            <IconButton onClick={addCategory}>
                                <AddIcon/>
                            </IconButton>
                            :
                            <IconButton onClick={editCategory}>
                                <DoneIcon/>
                            </IconButton>
                        }
                    </InputAdornment>,
                }}
            />
            <List>
                {categories.map(category => {
                    return (
                        <ListItem>
                            <ListItemText
                                primary={category.name}
                                secondary={'ID:' + category.ID}
                            />
                            <ListItemSecondaryAction>
                                <IconButton onClick={handleEdit(category)}>
                                    <EditIcon/>
                                </IconButton>
                                <IconButton onClick={delCategory(category.ID)}>
                                    <DeleteIcon/>
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    )
                })}
            </List>
        </div>
    )
}

export default Category