import React, { useState } from 'react'
import { Button, TextField } from '@material-ui/core'
import logo from '../logo.svg'
import Snackbar from '@material-ui/core/Snackbar'
import Paper from '@material-ui/core/Paper'
import Box from '@material-ui/core/Box'
import BG from '../assets/bg.jpg'
import InputAdornment from '@material-ui/core/InputAdornment'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import Email from '@material-ui/icons/Email'
import Face from '@material-ui/icons/Face'
import Lock from '@material-ui/icons/Lock'
import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import Typography from '@material-ui/core/Typography'
import { AdminApi } from '../api/admin'
import CircularProgress from '@material-ui/core/CircularProgress'
import makeStyles from '@material-ui/core/styles/makeStyles'

const useStyles = makeStyles(theme => ({
    signup: {
        height: 540,
        width: 350,
        zIndex: 4
    },
    header: {
        padding: '20px 0',
        marginTop: -40,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 15,
        background: 'linear-gradient(60deg, #6ec6ff, #0069c0)',
        color: 'white',
        '& > h5': {
            padding: theme.spacing(2)
        }
    },
    root: {
        display: 'flex',
        height: '100vh',
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundImage: `url(${BG})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center top',
        '&:before': {
            background: 'rgba(0, 0, 0, 0.5)'

        },
        '&:before,&:after': {
            position: 'absolute',
            zIndex: '1',
            width: '100%',
            height: '100%',
            display: 'block',
            left: '0',
            top: '0',
            content: '""'
        }
    },
    logo: {
        height: 50,
        pointerEvents: 'none'
    },
    input: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        padding: 30,
        paddingTop: 0,
        '& > div': {
            marginTop: 32
        }
    },
    ArrowBackButton: {
        zIndex: 99999,
        position: 'fixed',
        top: theme.spacing(),
        left: theme.spacing()
    }
}))

export default function Signup (props) {
    const classes = useStyles()

    const [email, setEmail] = useState('')
    const [username, setUsername] = useState('')
    const [nickname, setNickname] = useState('')
    const [password, setPassword] = useState('')
    const [againPassword, setAgainPassword] = useState('')
    const [showPassword, setShowPassword] = useState(false)
    const [loading, setLoading] = useState(false)
    const [snackbar, setSnackbar] = useState({open: false, message: ''})

    const handleSignup = () => {
        if (email === '') {
            setSnackbar({open: true, message: '邮箱不能为空'})
            return
        }

        if (nickname === '') {
            setSnackbar({open: true, message: '昵称不能为空'})
            return
        }

        if (username === '') {
            setSnackbar({open: true, message: '用户名不能为空'})
            return
        }

        if (password === '') {
            setSnackbar({open: true, message: '密码不能为空'})
            return
        }

        if (againPassword === '') {
            setSnackbar({open: true, message: '确认密码不能为空'})
            return
        }

        if (againPassword !== password) {
            setSnackbar({open: true, message: '两次密码不一致'})
            return
        }

        setLoading(true)

        AdminApi.signup(email,nickname,username,password)
            .then(data => {
                props.history.push('/login')
            })
            .catch(err => {
                setSnackbar({open:true,message: err.toString()})
                setLoading(false)
            })
    }

    return (
        <div className={classes.root}>
            <Snackbar
                anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                open={snackbar.open}
                onClose={() => setSnackbar({open: false, message: ''})}
                ContentProps={{
                    'aria-describedby': 'message-id',
                }}
                message={snackbar.message}
            />
            <Paper className={classes.signup} elevation='20'>
                <Tooltip title="返回">
                    <IconButton className={classes.ArrowBackButton}
                                onClick={() => props.history.goBack()}>
                        <ArrowBackIcon htmlColor="white"/>
                    </IconButton>
                </Tooltip>
                <Paper className={classes.header} elevation='15'>
                    <img src={logo} className={classes.logo} alt="logo"/>
                    <Typography variant="h5">
                        注册
                    </Typography>
                </Paper>
                <Box className={classes.input}>
                    <TextField
                        type='email'
                        onChange={(event) => setEmail(event.target.value)}
                        defaultValue={email}
                        InputProps={{
                            placeholder: '邮箱',
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Email/>
                                </InputAdornment>
                            )
                        }}/>
                    <TextField
                        onChange={(event) => setNickname(event.target.value)}
                        defaultValue={nickname}
                        InputProps={{
                            placeholder: '昵称',
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Face/>
                                </InputAdornment>
                            )
                        }}/>
                    <TextField
                        onChange={(event) => setUsername(event.target.value)}
                        defaultValue={username}
                        InputProps={{
                            placeholder: '用户名',
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Face/>
                                </InputAdornment>
                            )
                        }}/>
                    <TextField
                        type={showPassword ? 'text' : 'password'}
                        onChange={(event) => setPassword(event.target.value)}
                        defaultValue={password}
                        InputProps={{
                            placeholder: '密码',
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Lock/>
                                </InputAdornment>
                            ),
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton
                                        onClick={() => setShowPassword(!showPassword)}>
                                        {showPassword ? <Visibility/> : <VisibilityOff/>}
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}/>
                    <TextField
                        type={showPassword ? 'text' : 'password'}
                        onChange={(event) => setAgainPassword(event.target.value)}
                        defaultValue={againPassword}
                        InputProps={{
                            placeholder: '确认密码',
                            startAdornment: (
                                <InputAdornment position="start">
                                    <Lock/>
                                </InputAdornment>
                            )
                        }}/>
                </Box>
                {loading ? <CircularProgress size={31}/> :
                    <Button color="primary" size={'large'} onClick={handleSignup}>注册</Button>}
            </Paper>
        </div>
    )

}