import React, { Component } from 'react'
import { Button, TextField, withStyles } from '@material-ui/core'
import logo from '../logo.svg'
import Snackbar from '@material-ui/core/Snackbar'
import PropTypes from 'prop-types'
import Paper from '@material-ui/core/Paper'
import Box from '@material-ui/core/Box'
import BG from '../assets/bg.jpg'
import InputAdornment from '@material-ui/core/InputAdornment'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import Face from '@material-ui/icons/Face'
import Lock from '@material-ui/icons/Lock'
import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import Typography from '@material-ui/core/Typography'
import Cookies from 'js-cookie'
import { AdminApi } from '../api/admin'
import CircularProgress from '@material-ui/core/CircularProgress'

const styles = theme => ({
    login: {
        height: 350,
        width: 350,
        zIndex: 4
    },
    header: {
        padding: '20px 0',
        marginTop: -40,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 15,
        background: 'linear-gradient(60deg, #6ec6ff, #0069c0)',
        color: 'white',
        '& > h5': {
            padding: theme.spacing(2)
        }
    },
    root: {
        display: 'flex',
        height: '100vh',
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundImage: `url(${BG})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center top',
        '&:before': {
            background: 'rgba(0, 0, 0, 0.5)'

        },
        '&:before,&:after': {
            position: 'absolute',
            zIndex: '1',
            width: '100%',
            height: '100%',
            display: 'block',
            left: '0',
            top: '0',
            content: '""'
        }
    },
    logo: {
        height: 50,
        pointerEvents: 'none'
    },
    input: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        padding: 30,
        paddingTop: 0,
        '& > div': {
            marginTop: 32
        }
    },
    ArrowBackButton: {
        zIndex: 99999,
        position: 'fixed',
        top: theme.spacing(),
        left: theme.spacing()
    }
})

class Login extends Component {
    state = {
        username: '',
        password: '',
        snackbarOpen: false,
        snackbarText: '',
        showPassword: false,
        loading: false
    }

    handleLogin = () => {
        if (this.state.username === '') {
            this.setState({snackbarOpen: true, snackbarText: '用户名不能为空'})
            return
        }

        if (this.state.password === '') {
            this.setState({snackbarOpen: true, snackbarText: '密码不能为空'})
            return
        }

        this.setState({loading: true})

        AdminApi.login(this.state.username, this.state.password)
            .then(data => {
                Cookies.set('user', data.user)
                this.props.history.push('/')
            })
            .catch(err => {
                if (err.toString() === 'Error: username or password errors') {
                    this.setState({snackbarOpen: true, snackbarText: '用户名或密码错误', loading: false})
                } else {
                    this.setState({snackbarOpen: true, snackbarText: err.toString(), loading: false})
                }
            })
    }

    render () {
        const {classes} = this.props

        return (
            <div className={classes.root}>
                <Snackbar
                    anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                    open={this.state.snackbarOpen}
                    onClose={() => this.setState({snackbarOpen: false})}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={this.state.snackbarText}
                />
                <Paper className={classes.login} elevation='20'>
                    <Tooltip title="返回">
                        <IconButton className={classes.ArrowBackButton}
                                    onClick={() => this.props.history.goBack()}>
                            <ArrowBackIcon htmlColor="white"/>
                        </IconButton>
                    </Tooltip>
                    <Paper className={classes.header} elevation='15'>
                        <img src={logo} className={classes.logo} alt="logo"/>
                        <Typography variant="h5">
                            登录
                        </Typography>
                    </Paper>
                    <Box className={classes.input}>
                        <TextField
                            onChange={(event) => this.setState({username: event.target.value})}
                            defaultValue={this.state.username}
                            InputProps={{
                                placeholder: '用户名',
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Face/>
                                    </InputAdornment>
                                )
                            }}/>
                        <TextField
                            type={this.state.showPassword ? 'text' : 'password'}
                            onChange={(event) => this.setState({password: event.target.value})}
                            defaultValue={this.state.password}
                            InputProps={{
                                placeholder: '密码',
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <Lock/>
                                    </InputAdornment>
                                ),
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <IconButton
                                            onClick={() => this.setState({showPassword: !this.state.showPassword})}>
                                            {this.state.showPassword ? <Visibility/> : <VisibilityOff/>}
                                        </IconButton>
                                    </InputAdornment>
                                )
                            }}/>
                    </Box>
                    {this.state.loading ? <CircularProgress size={31}/> :
                        <Button color="primary" size={'large'} onClick={this.handleLogin}>登录</Button>}
                </Paper>
            </div>
        )

    }
}

Login.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(Login)