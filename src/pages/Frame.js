import React, { Suspense, useEffect, useState } from 'react'
import Button from '@material-ui/core/Button'
import { Link, Route, Switch } from 'react-router-dom'
import { makeStyles } from '@material-ui/core'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import MenuItem from '@material-ui/core/MenuItem'
import IconButton from '@material-ui/core/IconButton'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import Menu from '@material-ui/core/Menu'
import Box from '@material-ui/core/Box'
import Cookies from 'js-cookie'
import { AdminApi } from '../api/admin'
import LinearProgress from '@material-ui/core/LinearProgress'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import { AddressApi } from '../api/address'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import DialogActions from '@material-ui/core/DialogActions'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import Avatar from '@material-ui/core/Avatar'
import AddIcon from '@material-ui/icons/Add'
import TextField from '@material-ui/core/TextField'
import DeleteIcon from '@material-ui/icons/Delete'
import Snackbar from '@material-ui/core/Snackbar'

const Takeaway = React.lazy(() => import('./Takeaway'))
const Home = React.lazy(() => import('./Home'))
const Order = React.lazy(() => import('./Order'))

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    grow: {
        flexGrow: 1,
        textAlign: 'left'
    },
    toolbar: theme.mixins.toolbar,
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
    appHomeBar: {
        zIndex: theme.zIndex.drawer + 1,
        backgroundColor: 'transparent'
    },
    content: {
        flexGrow: 1,
    },
    addressManager: {
        minWidth: 500
    }
}))

function Frame (props) {
    const classes = useStyles()

    const [user, setUser] = useState(null)
    const [anchorEl, setAnchorEl] = useState(null)
    const [tabValue, setTabValue] = useState(0)
    const [dialogOpen, setDialogOpen] = useState(false)
    const [addressDialogOpen, setAddressDialogOpen] = useState(false)
    const [newAddress, setNewAddress] = useState({})
    const [address, setAddress] = useState([])
    const [snackbar, setSnackbar] = useState({open: false, message: ''})

    const getAddress = () => AddressApi.getAddress().then(data => {setAddress(data.address)})

    const router = ['/', '/takeaway', '/order']

    useEffect(() => {
        let user = Cookies.getJSON('user')
        if (user !== undefined) {
            setUser(user)
            getAddress()
        }
    }, [])

    useEffect(() => {
        setTabValue(router.indexOf(props.location.pathname))
    }, [props.location.pathname])

    const handleMenuClose = index => () => {
        switch (index) {
            case 0:
                props.history.push('/admin')
                break
            case 1:
                AdminApi.logout()
                    .then(() => {
                        Cookies.remove('user')
                        setUser(null)
                    })
                    .catch(err => {
                        alert('退出失败:' + err.toString())
                    })
                break
        }

        setAnchorEl(null)
    }

    const hasAdminGroup = () => {
        for (let i = 0; i < user.Group.length; i++) {
            if (user.Group[i].Name === 'admin') {
                return true
            }
        }
        return false
    }

    const handleChange = (event, value) => props.history.push(router[value])

    const handleAddAddress = () => {
        AddressApi.addAddress(newAddress).then(() => {
            getAddress()
                .then(() => {
                    setSnackbar({open: true, message: '添加成功'})
                    getAddress()
                })
                .catch(err => setSnackbar({open: true, message: err.toString()}))
        })
        
        setAddressDialogOpen(false)
    }

    const delAddress = id => () => {
        AddressApi.deleteAddress(id)
            .then(() => {
                setSnackbar({open: true, message: '删除成功'})
                getAddress()
            })
            .catch(err => setSnackbar({open: true, message: err.toString()}))
    }

    return (
        <div className={classes.root}>
            <Snackbar
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={snackbar.open}
                autoHideDuration={4000}
                onClose={() => setSnackbar({open: false, message: ''})}
                ContentProps={{
                    'aria-describedby': 'message-id',
                }}
                message={<span id="message-id">{snackbar.message}</span>}
            />
            <AppBar position="fixed" className={tabValue === 0 ? classes.appHomeBar : classes.appBar}>
                <Toolbar>
                    <Typography variant="h6" color="inherit" className={classes.grow}>
                        网上酒楼外卖
                    </Typography>
                    <Tabs
                        value={tabValue}
                        onChange={handleChange}
                    >
                        <Tab label="首页"/>
                        <Tab label="外卖"/>
                        {user != null && <Tab label="我的订单"/>}
                    </Tabs>
                    {user === null ?
                        <div>
                            <Button color="inherit" component={Link} to={'/signup'}>注册</Button>
                            <Button color="inherit" variant='outlined' component={Link} to={'/login'}>登录</Button>
                        </div> :
                        <Box display='flex' alignItems="center" pl={3}>
                            <Typography variant="h6">{user.Username}</Typography>
                            <IconButton
                                aria-haspopup="true"
                                onClick={event => {setAnchorEl(event.currentTarget)}}
                                color="inherit"
                            >
                                <MoreVertIcon/>
                            </IconButton>
                            <Menu
                                anchorEl={anchorEl}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={Boolean(anchorEl)}
                                onClose={handleMenuClose()}
                            >
                                {hasAdminGroup() && <MenuItem onClick={handleMenuClose(0)}>后台管理</MenuItem>}
                                <MenuItem onClick={() => setDialogOpen(true)}>地址管理</MenuItem>
                                <MenuItem onClick={handleMenuClose(1)}>注销</MenuItem>
                            </Menu>
                        </Box>}
                </Toolbar>
            </AppBar>
            <main className={classes.content}>
                {tabValue !== 0 && <div className={classes.toolbar}/>}
                <Suspense fallback={<LinearProgress/>}>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route path="/takeaway" component={Takeaway}/>
                        <Route path="/order" component={Order}/>
                    </Switch>
                </Suspense>
            </main>
            {user !== null && <Dialog
                open={dialogOpen}
                onClose={() => setDialogOpen(true)}>
                <DialogTitle>{'地址管理'}</DialogTitle>
                <DialogContent className={classes.addressManager}>
                    <List>
                        {address.map(address => {
                                return (
                                    <ListItem>
                                        <ListItemText
                                            primary={address.address}
                                            secondary={'收货人:' + address.name}
                                        />
                                        <ListItemSecondaryAction>
                                            <IconButton onClick={delAddress(address.ID)}>
                                                <DeleteIcon/>
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                )
                            }
                        )}
                        <ListItem autoFocus button onClick={() => setAddressDialogOpen(true)}>
                            <ListItemAvatar>
                                <Avatar>
                                    <AddIcon/>
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary="添加新配送地址"/>
                        </ListItem>
                    </List>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setDialogOpen(false)} color="primary"
                            autoFocus>
                        关闭
                    </Button>
                </DialogActions>
            </Dialog>}
            {user !== null && <Dialog open={addressDialogOpen} onClose={() => setAddressDialogOpen(false)}>
                <DialogTitle>添加新配送地址</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin="dense"
                        label="姓名"
                        type="text"
                        fullWidth
                        defaultValue={newAddress.name}
                        onChange={(event) => setNewAddress({...newAddress, name: event.target.value})}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        label="年龄"
                        type="number"
                        fullWidth
                        defaultValue={newAddress.age}
                        onChange={(event) => setNewAddress({...newAddress, age: event.target.value})}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        label="手机号"
                        type="number"
                        fullWidth
                        defaultValue={newAddress.phone}
                        onChange={(event) => setNewAddress({...newAddress, phone: event.target.value})}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        label="地址"
                        type="text"
                        fullWidth
                        defaultValue={newAddress.address}
                        onChange={(event) => setNewAddress({...newAddress, address: event.target.value})}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setAddressDialogOpen(false)} color="primary">
                        取消
                    </Button>
                    <Button onClick={handleAddAddress} color="primary">
                        添加
                    </Button>
                </DialogActions>
            </Dialog>}
        </div>
    )
}

export default Frame