import React, { useEffect, useState } from 'react'
import Paper from '@material-ui/core/Paper'
import { OrderApi } from '../api/order'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'

import wechatpay from '../assets/wechatpay.png'
import alipay from '../assets/alipay.jpg'
import CircularProgress from '@material-ui/core/CircularProgress'
import Snackbar from '@material-ui/core/Snackbar'

const useStyles = makeStyles(theme => ({
    root: {
        textAlign: 'left',
        padding: theme.spacing(3)
    },
    order: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: theme.spacing(3),
        marginBottom: theme.spacing(3)
    },
    orderAction: {
        '& > button': {
            marginLeft: 10
        }
    },
    payImg: {
        display: 'flex',
        '& > img': {
            width: 250,
            marginLeft: theme.spacing(),
            marginRight: theme.spacing()
        }
    }
}))

function Order (props) {
    const classes = useStyles()
    const timer = React.useRef()

    const [orders, setOrders] = useState([])
    const [payDialog, setPayDialog] = useState(false)
    const [payPrice, setPayPrice] = useState(0)
    const [waitPay, setWaitPay] = useState(false)
    const [payID, setPayID] = useState(0)
    const [snackbar,setSnackbar] = useState({open:false,message:''})

    const orderStatus = ['等待付款', '等待配送', '已完成', '已取消']

    useEffect(() => {
        getOrders()
    }, [])

    const getOrders = () => OrderApi.getOrders().then(data => setOrders(data.orders))

    function calcPrice (foods) {
        let count = 0
        for (let i = 0; i < foods.length; i++) {
            count += foods[i].price
        }

        return count
    }

    const handlePayButton = (price, id) => () => {
        setPayPrice(price)
        setPayID(id)
        setPayDialog(true)
    }

    const handleDialogPayButton = () => {
        setWaitPay(true)
        OrderApi.pay(payID).then(() => {
            timer.current = setTimeout(() => {
                setWaitPay(false)
                setPayDialog(false)
                getOrders()
                setSnackbar({open: true,message:'付款成功'})
            }, 3000)
        })
            .catch(err => {
                setSnackbar({open: true,message:err.toString()})
            })
    }

    const handleReceiptButton = id => () => {
        OrderApi.complete(id)
            .then(() => {
                getOrders()
                setSnackbar({open: true,message:'收货完成'})
            })
            .catch(err => {
                setSnackbar({open: true,message:err.toString()})
            })
    }

    const handleCancelButton = id => () => {
        OrderApi.cancel(id)
            .then(() => {
                getOrders()
                setSnackbar({open: true,message:'取消成功'})
            })
            .catch(err => {
                setSnackbar({open: true,message:err.toString()})
            })
    }

    const handleDeleteButton = id => () => {
        OrderApi.delete(id)
            .then(() => {
                getOrders()
                setSnackbar({open: true,message:'取消成功'})
            })
            .catch(err => {
                setSnackbar({open: true,message:err.toString()})
            })
    }

    return (
        <div className={classes.root}>
            <Snackbar
                anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                open={snackbar.open}
                onClose={() => setSnackbar({open:false,message:''})}
                ContentProps={{
                    'aria-describedby': 'message-id',
                }}
                message={snackbar.message}
            />
            <Dialog onClose={() => setPayDialog(false)} open={payDialog}>
                <DialogTitle onClose={() => setPayDialog(false)}>
                    请付款 ￥ {payPrice}
                </DialogTitle>
                <DialogContent dividers className={classes.payImg}>
                    <img className={classes.payImg} src={alipay} alt=""/>
                    <img className={classes.payImg} src={wechatpay} alt=""/>
                </DialogContent>
                <DialogActions>
                    {waitPay ?
                        <div>等待确认 <CircularProgress/></div>
                        : <Button autoFocus onClick={handleDialogPayButton} color="primary">
                            我已支付
                        </Button>
                    }
                </DialogActions>
            </Dialog>
            {orders.map(order => (
                <Paper className={classes.order}>
                    <Box display='flex' flexDirection='column'>
                        <Typography>订单号: {order.ID}</Typography>
                        <Typography>状态: {orderStatus[order.status]}</Typography>
                    </Box>
                    <Box>
                        <Typography>
                            订单中的食物:
                            {order.foods.map(food => (food.name + ','))}
                        </Typography>
                    </Box>
                    <Box display='flex' flexDirection='column' alignItems='flex-end'>
                        <Typography>价格: ￥ {calcPrice(order.foods)}</Typography>
                        <Box className={classes.orderAction}>
                            {order.status === 0 && <Button variant={'contained'} color='primary'
                                                           onClick={handlePayButton(calcPrice(order.foods), order.ID)}>付款</Button>}
                            {order.status === 0 && <Button variant={'contained'} color='primary' onClick={handleCancelButton(order.ID)}>取消订单</Button>}
                            {order.status === 1 && <Button variant={'contained'} color='primary' onClick={handleReceiptButton(order.ID)}>确定收货</Button>}
                            {(order.status === 2 || order.status === 3) && <Button variant={'contained'} color='primary' onClick={handleDeleteButton(order.ID)}>删除订单</Button>}
                        </Box>
                    </Box>
                </Paper>
            ))}
        </div>
    )
}

export default Order