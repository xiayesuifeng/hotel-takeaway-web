import React, { useEffect, useReducer, useState } from 'react'
import { makeStyles } from '@material-ui/core'
import Drawer from '@material-ui/core/Drawer'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import { CategoryApi } from '../api/category'
import { FoodApi } from '../api/food'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'
import Box from '@material-ui/core/Box'
import Fab from '@material-ui/core/Fab'
import Cookies from 'js-cookie'
import Food from '../component/Food'
import ShoppingCart from '../component/ShoppingCart'

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        textAlign: 'left'
    },
    tabs: {
        borderRight: `1px solid ${theme.palette.divider}`,
    },
    content: {
        flexGrow: 1,
    },
    toolbar: theme.mixins.toolbar,
    drawer: {
        width: 160,
        flexShrink: 0,
    },
    drawerPaper: {
        width: 160,
    },
    fab: {
        zIndex: 99999,
        position: 'fixed',
        bottom: theme.spacing(3),
        right: theme.spacing(3),
        margin: theme.spacing(1),
    },
    fabIcon: {
        marginRight: theme.spacing(1),
    },
}))

export const ShoppingCartContext = React.createContext()

export default function Takeaway (props) {
    const classes = useStyles()

    const [tabValue, setTabValue] = useState(0)
    const [categories, setCategories] = useState([])
    const [foods, setFoods] = useState([])
    const [showShoppingCart, setShowShoppingCart] = useState(false)

    const getCategories = () => CategoryApi.getCategories().then(data => setCategories(data.categories))
    const getFoods = () => FoodApi.getFoods().then(data => setFoods(data.foods))

    const cartReducer = (state, action) => {
        console.log(action.type)
        switch (action.type) {
            case 'ADD_FOOD':
                state = [...state, action.data]
                Cookies.set('shoppingCart', {'list': state})
                return state
            case 'REMOVE_FOOD':
                state.splice(action.data, 1)
                Cookies.set('shoppingCart', {'list': state})
                return state
            case 'RESET':
                Cookies.remove('shoppingCart')
                return []
            default:
                return state
        }
    }

    const initShoppingCart = () => {
        let tmp = Cookies.getJSON('shoppingCart')
        if (tmp === undefined)
            tmp = {'list': []}

        return tmp.list
    }

    const [shoppingCart, shoppingCartDispatch] = useReducer(cartReducer, [],initShoppingCart)

    useEffect(() => {
        shoppingCartDispatch({type: 'INIT_FOOD'})

        getCategories()
        getFoods()
    }, [])

    useEffect(() => {
        if (tabValue === 0)
            getFoods()
        else {
            FoodApi.getFoodsByCategory(categories[tabValue - 1].ID).then(data => setFoods(data.foods))
        }
    }, [tabValue])

    const handleChange = (event, newValue) => {
        setTabValue(newValue)
    }

    return (
        <div className={classes.root}>
            <Drawer
                className={classes.drawer}
                variant="permanent"
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div className={classes.toolbar}/>
                <Tabs
                    orientation="vertical"
                    variant="scrollable"
                    value={tabValue}
                    onChange={handleChange}
                    className={classes.tabs}
                >
                    <Tab label="全部"/>
                    {categories.map(category => <Tab label={category.name}/>)}
                </Tabs>
            </Drawer>
            <ShoppingCartContext.Provider value={{
                shoppingCart: shoppingCart,
                shoppingCartDispatch: shoppingCartDispatch
            }}>
                <main className={classes.content}>
                    <Box display="flex" flexWrap="wrap">
                        {foods.map(food => <Food food={food}/>)}
                    </Box>
                    {showShoppingCart ? <ShoppingCart history={props.history}
                                                      onClose={() => setShowShoppingCart(false)}/>
                        : <Fab variant="extended" className={classes.fab}
                               onClick={() => setShowShoppingCart(!showShoppingCart)}><ShoppingCartIcon
                            className={classes.fabIcon}/>购物车</Fab>}
                </main>
            </ShoppingCartContext.Provider>
        </div>
    )
}