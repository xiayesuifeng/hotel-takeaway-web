import Button from '@material-ui/core/Button'
import { Link } from 'react-router-dom'
import Box from '@material-ui/core/Box'
import React from 'react'
import { makeStyles } from '@material-ui/core'

import BG from '../assets/home_bg.jpg'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles(theme => ({
    logo: {
        height: 300,
        [theme.breakpoints.down('sm')]: {
            width: '100%'
        },
        [theme.breakpoints.down('xs')]: {
            width: '100%',
            height: 150
        }
    },
    root: {
        '& h1, h2': {fontSize: 25, lineHeight: 25},
        '& h3, h4': {fontSize: 20, lineHeight: 20},
        '& h5, h6': {fontSize: 16, lineHeight: 16},
        '& h1,h2,h3,h4,h5,h6': {
            letterSpacing: 1, fontWeight: 700, lineHeight: 1.2
        },
        '& p': {margin: '10px 0', color: '#777'}
    },
    header: {
        background: `url(${BG}) no-repeat center top;color:#fff`,
        minHeight: 950,
        backgroundSize: 'cover',
        '& .main-header': {
            textAlign: 'center',
            paddingTop: 100
        },
        '& .header-text': {
            color: '#fff',
            textShadow: '2px 2px #111'
        },
        '& .header-text span': {
            fontFamily: 'Satisfy',
            cursive: true,
            color: '#e6be99',
            lineHeight: 1.2,
            fontSize: 46,
            marginBottom: 14,
            display: 'block'
        },
        '& .header-text h1': {
            marginTop: 0, marginBottom: 0, textTransform: 'uppercase', color: '#fff',
            fontSize: 44, letterSpacing: 5
        },
        '& .header-text p': {
            marginBottom: 50, fontSize: 22, color: '#fff', lineHeight: '1.6'
        },
        '& .header-text .btn-primary': {
            backgroundColor: '#EC495E',
            border: 'none',
            padding: '15px 33px',
            borderRadius: 3,
            fontWeight: 700,
            fontSize: 17,
            color: '#fff',
            textShadow: 'none'
        }
    },
    info: {
        textAlign: 'left',
        margin: 15
    },
    copyright: {
        display: 'flex',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        padding: 15,
        paddingLeft: 400,
        paddingRight: 400,
        background: '#000',
        color: '#999'
    },
    footer: {
        background: '#181819',
        fontSize: 13,
        '& .widget-title': {
            textTransform: 'capitalize', marginBottom: 30, color: '#fff'
        },
        '& p': {color: '#999'},
        '& .wrap-footer': {padding: '50px 60px 60px'},
        '& .wrap-footer ul li': {marginBottom: 5, padding: '10px 0 15px'}
    },
    footerInfo: {
        width: 300,
        marginRight: 20
    },
    footerInfo2: {
        width: 300,
        marginRight: 20,
        '& ul': {padding: 0},
        '& ul li': {listStyle: 'none', marginBottom: 5, borderBottom: '1px solid #999'},
        '& ul li:first-child': {paddingTop: 0},
        '& ul li:last-child': {borderBottom: 'none'},
        '& ul li a': {color: '#999', fontSize: 12},
        '& ul li a:hover': {color: '#ec495e'},
    },
    btn: {
        color: '#fff',
        fontSize: 17,
        fontWeight: 700,
        textShadow: 'none',
        marginTop: 100
    },
    a: {
        color: '#ec495e',
        textDecoration: 'none', MozTransition: '0.3s',
        MsTransition: '0.3s',
        OTransition: '0.3s',
        transitionDuration: '0.3s',
        '&:hover': {
            color: '#ec495e'
        }
    },
}))

function Home () {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <div className={classes.header}>
                <Box diisplay='flex' justifyContent='center' flexDirection='column' pt={30} className="header-text">
                    <Typography variant="caption">外卖订餐</Typography>
                    <Typography variant="h2">欢 迎 光 临！</Typography>
                    <Button className={classes.btn} color="secondary" variant='contained' component={Link}
                            to="/takeaway">我要订餐</Button>
                </Box>
            </div>
            <footer className={classes.footer}>
                <Box display='flex' justifyContent='center' flexWrap="wrap" p={5} className="wrap-footer">
                    <div className={classes.footerInfo}>
                        <h3 className="widget-title">关于我们</h3>
                        <Typography variant='body1'>截止至目前，我们已有八大菜系，多钟特色菜上架</Typography>
                    </div>

                    <div className={classes.footerInfo2}>
                        <h3 className="widget-title">服务中心</h3>
                        <ul>
                            <li><Typography variant='body2' className={classes.a}>联系我们</Typography></li>
                            <li><Typography variant='body2' className={classes.a}>门店招聘</Typography></li>
                        </ul>
                    </div>
                    <div className={classes.footerInfo}>
                        <h3 className="widget-title">特色美食</h3>
                        <Typography variant='body2'>粤菜菜系</Typography>
                        <Typography variant='body2'>川菜菜系</Typography>
                        <Typography variant='body2'>鲁菜菜系</Typography>
                    </div>
                </Box>
            </footer>
        </div>
    )
}

export default Home