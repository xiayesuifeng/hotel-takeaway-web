# hotel-takeaway-web

> 网上酒楼外卖系统前端

![license](https://img.shields.io/badge/license-GPL3.0-green.svg)

## 后端

[hotel-takeaway](https://gitlab.com/xiayesuifeng/hotel-takeaway.git)

## 编译

```
git clone https://gitlab.com/xiayesuifeng/hotel-takeaway-web.git
npm run install
npm run build
```

[详细部署](https://gitlab.com/xiayesuifeng/hotel-takeaway/blob/master/README.md)

## License

hotel-takeaway-web is licensed under [GPLv3](LICENSE).
